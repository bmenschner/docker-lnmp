# docker-lnmp stack
Docker Live and Dev Environment with minimal dependancies. 
Separated Live and Dev Stack. 

## Inhaltsverzeichnis
- Live Environment Definiton [link](#live)
- Dev Environment Definiton [link](#dev)
- Startup Guide [link](#startup)
- Setup Guide Xdebug for Intellj Idea [link](#xdebug)
- Setup Guide PHP Unit for Intellj Idea [link](#phpunit)

## <a id="live"> Live Environment </a>
- NGINX
- PHP
  - pdo 
  - pdo_mysql
  - mysqli
- Maria DB

## <a id="dev"> Dev Environment </a>
- NGINX
- PHP
  - pdo 
  - pdo_mysql
  - mysqli
  - phpunit
  - xdebug
- Maria DB 

## <a id="startup"> Startup Guide </a>
Live Environment:
- open terminal
- docker-compose up -d
- open "localhost:8080" in your favorite browser

Dev Environment:
- open terminal
- docker-compose -f docker-compose.dev.yml up -d
- open "localhost:8080" in your favorite browser

##Setup Guide Xdebug for Intellj Idea
Todo

##Setup Guide PHP Unit for Intellj Idea
Todo