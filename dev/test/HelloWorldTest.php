<?php

/** Hello World PHP Unit Test without dependencies */

use PHPUnit\Framework\TestCase;

class HelloWorldTest extends TestCase {

    /** Test Function with fixed values */

    public function testGreeting() {

        $greeting = "Hello World";
        $requiredGreeting = "Hello World";

        $this->assertEquals($greeting, $requiredGreeting);

    }

}

?>