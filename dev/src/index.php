<?php

include_once ("index.html");

/** PHP Test */

class Message {
    public $message;

    /** Constructor Method*/
    function __construct($input)
    {
        $this->message = $input;
    }

    /** Setter Method - Set Value for $greetings*/

    function setMessage($new_input)
    {
        $this->message = $new_input;
    }

    /** Getter Method - Return the Value */

    function getMessage()
    {
        return $this->message;
    }
}

$message = new Message('');

/** Test PHP Functionality and prompt status in index.php*/
echo"<h1>PHP Support</h1>";

$message->setMessage('<p class="valid">PHP Support: OK</p>');
echo $message->getMessage();

/** Test MySQL / MariaDB Connection */
echo"<h1>MySQL Support for PHP</h1>";

try{
    $dbh = new pdo( 'mysql:host=mariadb;dbname=database', 'user', 'password',
        array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
    echo "<p class='valid'>";
    die(json_encode(array('message' => 'Mysql Status: OK - Connection Successfull!')));
    echo "</p>";
}
catch(PDOException $ex){
    echo "<p class='failed'>";
    die(json_encode(array('message' => 'Mysql Status: Failed - Unable to connect')));
    echo "</p>";
}

phpinfo();

?>