<?php

/** Hello World PHP Unit Test with index.php dependency */

use PHPUnit\Framework\TestCase;
include '/var/www/html/index.php';

class HelloWorld2Test extends TestCase {

    public function testGreeting() {

        /** Create Object $helloWorld from index.php */
        $helloWorld = new HelloWorld('');

        /** Set $helloWorld */
        $helloWorld->setGreeting('Hello World');

        /** Use return value from $helloWorld for $greeting */
        $greeting = $helloWorld->getGreeting();

        /** $requiredGreeting */
        $requiredGreeting = "Hello World";

        /** Use $requiredGreeting to compare with $greeting */
        $this->assertEquals($greeting, $requiredGreeting);

    }

}

?>